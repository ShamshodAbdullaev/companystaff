$(document).ready(function () {
    $('#country').change(function () {
        getRegions($(this).val());
    })
    function getRegions(id) {
        $.ajax({
            url: "/regions",
            data: {id: id},
            type: "GET",
            success: function (regions) {
                $('#region').empty();
                $('#region').append('<option selected disabled>Viloyatni tanlang</option>');
                $.each(regions, function (i, region) {
                    $('#region').append($("<option/>", {
                        value: region.id,
                        text: region.name
                    }));
                });
            },
            error: function (jqXHR, exception) {
                console.log(error);
            }
        })
    }


    $('#region').change(function () {
        getDistrict($(this).val());
    })
    function getDistrict(id) {
        $.ajax({
            url: "/districts",
            data: {id: id},
            type: "GET",
            success: function (districts) {
                $('#distrtict').empty();
                $('#distrtict').append('<option selected disabled>Tumanni tanlang</option>');
                $.each(districts, function (i, district) {
                    $('#district').append($("<option/>", {
                        value: district.id,
                        text: district.name
                    }));
                });
            },
            error: function (jqXHR, exception) {
                console.log(error);
            }
        })
    }
})