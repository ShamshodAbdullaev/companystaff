
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html lang="en">


<!-- Mirrored from www.themeon.net/nifty/v2.9.1/pages-register.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 06 Feb 2019 07:26:00 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Register page | Nifty - Admin Template</title>


    <!--STYLESHEET-->
    <!--=================================================-->

    <!--Open Sans Font [ OPTIONAL ]-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>


    <!--Bootstrap Stylesheet [ REQUIRED ]-->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">


    <!--Nifty Stylesheet [ REQUIRED ]-->
    <link href="/assets/css/nifty.min.css" rel="stylesheet">


    <!--Nifty Premium Icon [ DEMONSTRATION ]-->
    <link href="/assets/css/demo/nifty-demo-icons.min.css" rel="stylesheet">


    <!--=================================================-->


    <!--Pace - Page Load Progress Par [OPTIONAL]-->
    <link href="/assets/plugins/pace/pace.min.css" rel="stylesheet">
    <script src="/assets/plugins/pace/pace.min.js"></script>
    <link href="/assets/css/demo/nifty-demo-icons.min.css" rel="stylesheet">
    <link href="/assets/plugins/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">


    <!--Demo [ DEMONSTRATION ]-->
    <link href="/assets/css/demo/nifty-demo.min.css" rel="stylesheet">


    <!--=================================================

    REQUIRED
    You must include this in your project.


    RECOMMENDED
    This category must be included but you may modify which plugins or components which should be included in your project.


    OPTIONAL
    Optional plugins. You may choose whether to include it in your project or not.


    DEMONSTRATION
    This is to be removed, used for demonstration purposes only. This category must not be included in your project.


    SAMPLE
    Some script samples which explain how to initialize plugins or components. This category should not be included in your project.


    Detailed information and more samples can be found in the document.

    =================================================-->

</head>

<!--TIPS-->
<!--You may remove all ID or Class names which contain "demo-", they are only used for demonstration. -->

<body>
<div id="container" class="cls-container">
    <li class="mega-dropdown list-unstyled " style="float: right ">

        <a href="?lang=uz"><span class="flag-icon flag-icon-uz"></span> O`zbekcha</a>
        <a href="?lang=en"><span class="flag-icon flag-icon-us"></span> English</a>
        <a href="?lang=ru"><span class="flag-icon flag-icon-ru"></span> Русский</a>


    </li>


    <!-- BACKGROUND IMAGE -->
    <!--===================================================-->
    <div id="bg-overlay"></div>


    <!-- REGISTRATION FORM -->
    <!--===================================================-->
    <div class="content">
        <div class="content-lg panel">
            <div class="panel-body">
                <div class="mar-ver pad-btm">
                    <h1 class="h3">Create a New Company</h1>
                    <p>Come join the Company Staff community! Let's set up your account.</p>
                </div>
                <form action="/signup" method="post">
                   <div class="row">
                       <div class="form-group col-md-4">
                           <label for="login">Login <sup class="text-danger">*</sup></label>
                           <input type="text" class="form-control" placeholder="Login" name="login" id="login"
                                  required>
                       </div>
                       <div class="form-group col-md-4">
                           <label for="password">Parolni kiriting<sup class="text-danger">*</sup></label>
                           <input type="password" class="form-control" placeholder="Parolni kiriting"
                                  name="password" id="password" required>
                       </div>
                       <div class="form-group col-md-4">
                           <label for="prepassword">Parolni qayta kiriting<sup class="text-danger">*</sup></label>
                           <input type="password" class="form-control" placeholder="Parolni qayta kiriting"
                                  name="prepassword" id="prepassword" required>
                       </div>
                   </div>
                    <div class="row text-left ">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-3"><div class="form-group">
                                <label for="name" class="">Kompaniya nomi <sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" placeholder="Kompaniya nomi" name="name"
                                       id="name" required>
                            </div></div>
                                <div class="col-md-3"><div class="form-group">
                                <label for="business_type">Faoliyat turini tanlang <sub
                                        class="text-danger">*</sub></label>
                                <input type="text" class="form-control" placeholder="Faoliyat turini tanlang"
                                       name="business_type" id="business_type" required>
                            </div></div>
                                <div class="col-md-3"><div class="form-group">
                                    <label for="business_entity">Korxona shakli<sup class="text-danger">*</sup></label>
                                <select type="text" class="form-control" name="business_entity" id="business_entity" required>
                                    <option selected disabled>Korxona shaklini tanlang</option>
                                    <c:forEach items="${businessEntities}" var="businesEntity">
                                        <option value="${businesEntity.id}">${businesEntity.type}</option>
                                    </c:forEach>
                                </select>
                            </div></div>
                                <div class="col-md-3"><div class="form-group">
                                <label for="inn">INN <sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" placeholder="INN kiriting" name="inn" id="inn"
                                       required>
                            </div></div>

                            </div>
                            <div class="row">
                                <div class="col-md-3"><div class="form-group">
                                <label for="bank_mfo">Bank MFO <sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" placeholder="Bank MFO ni kiriting"
                                       name="bank_mfo" id="bank_mfo" required>
                            </div></div>
                                <div class="col-md-3"><div class="form-group">
                                <label for="account">Bank hisob raqami <sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" placeholder="Bank hisob raqamni kiriting"
                                       name="account" id="account" required>
                            </div></div>
                                <div class="col-md-3"><div class="form-group">
                                <label for="start_date">Tashkil etilgan sanasi <sup class="text-danger">*</sup></label>
                                <input type="date" class="form-control" placeholder="Tashkil etilgan sanasi"
                                       name="start_date" id="start_date" required>
                            </div></div>
                                <div class="col-md-3"><div class="form-group">
                                <label for="country">Mamlakat nomi tanlang <sup class="text-danger">*</sup></label>
                                    <select class="form-control"
                                            name="country" id="country" required >
                                    <option selected disabled>Mamlakat nomi tanlang</option>
                                    <c:forEach items="${countries}" var ="country">
                                        <option value="${country.id}">${country.name}</option>
                                    </c:forEach>
                                </select>
                            </div></div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                <label for="region">Viloyatni tanlang <sup class="text-danger">*</sup></label>
                                <select  class="form-control" placeholder="Viloyatni tanlang" name="region"
                                        id="region" required>
                                    <option selected disabled>Viloyatni tanlang</option>
                                </select>
                            </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                <label for="city">Shaharni nomi <sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" placeholder="Shaharni tanlang" name="city"
                                       id="city" required>
                            </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="district">Tuman nomini tanlang <sup class="text-danger">*</sup></label>
                                        <select name="district" id="district" class="form-control" required>
                                            <option selected disabled>Tuman nomini tanlang</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                <label for="street">Ko'cha nomini kiriting <sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" placeholder="Ko'cha nomini kiriting"
                                       name="street" id="street">
                            </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                <label for="home">Manzilni kiriting <sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" placeholder="Manzilni kiriting" name="home"
                                       id="home">
                            </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                <label for="zip_code">Indeks <sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" placeholder="Pochta indeksini kiriting"
                                       name="zip_code" id="zip_code">
                            </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                <label for="phone_number">Telefon raqamni kiriting <sub
                                        class="text-danger">*</sub></label>
                                <input type="text" class="form-control" placeholder="Telefon raqamni kiriting"
                                       name="phone_number" id="phone_number">
                            </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                <label for="fax">Faks raqamini kiriting <sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" placeholder="Faks raqamini kiriting" name="fax_number"
                                       id="fax">
                            </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-3 col-md-offset-3">
                                    <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" placeholder="E-mailni kiriting" name="email"
                                       id="email">
                            </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                <label for="web_site">Web sayt</label>
                                <input type="url" class="form-control" placeholder="Web sayt" name="website"
                                       id="web_site">
                            </div>
                                </div>
                            </div>
                        </div>
                        </div>

                    <div class="checkbox pad-btm text-left">
                        <input id="demo-form-checkbox" class="magic-checkbox" type="checkbox">
                        <label for="demo-form-checkbox">I agree with the <a href="/assets/#" class="btn-link text-bold">Terms
                            and Conditions</a></label>
                    </div>
                    <button class="btn btn-primary btn-lg" type="submit">Register</button>
                </form>
            </div>
            <div class="pad-all">
                Already have an account ? <a href="/login" class="btn-link mar-rgt text-bold">Sign In</a>


            </div>
        </div>
    </div>
    <!--===================================================-->


</div>
<!--===================================================-->
<!-- END OF CONTAINER -->


<!--JAVASCRIPT-->
<!--=================================================-->

<!--jQuery [ REQUIRED ]-->
<script src="/assets/js/jquery.min.js"></script>


<!--BootstrapJS [ RECOMMENDED ]-->
<script src="/assets/js/bootstrap.min.js"></script>


<!--NiftyJS [ RECOMMENDED ]-->
<script src="/assets/js/nifty.min.js"></script>


<!--=================================================-->

<!--Background Image [ DEMONSTRATION ]-->
<script src="/assets/js/demo/bg-images.js"></script>
<script src="/assets/js/sign/signup.js"></script>
<script src="/assets/bootstrap4/js/jQuery.min.js"></script>
<script src="/assets/bootstrap4/js/src/popover.js"></script>
<script src="/assets/bootstrap4/js/bootstrap.js"></script>

</body>

<!-- Mirrored from www.themeon.net/nifty/v2.9.1/pages-register.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 06 Feb 2019 07:26:00 GMT -->
</html>

