
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>header</title>
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
</head>
<body>
<header>
    <div class="row">
        <div class="col-md-3">
            <img src="../../img/photo_2019-02-06_11-53-54.jpg" alt="">
        </div>
        <div class="col-md-6">
            <h1 class="text-center">PDP</h1>
        </div>
        <div class="col-md-3">
            <div class="row">
                <div class="col-md form-group">
                    <select name="subsub" class="form-control">
                        <option selected disabled>language</option>
                        <option value="1">uz</option>
                        <option value="2">ru</option>
                    </select>
                </div>
                <div class="col-md">
                    <a href="#" class="btn btn-block btn-success">registration</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md">
                    <div class="row">
                        <div class="col-md-6">
                            <input type="text" placeholder="login" name="login">
                        </div>
                        <div class="col-md-6">
                            <input type="text" placeholder="password" name="password">
                        </div>
                    </div>
                    <a href="#" class="btn btn-block btn-success">sign in</a>
                </div>
            </div>
        </div>
    </div>
</header>
<script src="../../js/bootstrap.min.js"></script>
</body>
</html>

