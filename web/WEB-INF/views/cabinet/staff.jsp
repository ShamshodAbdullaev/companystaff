<div class="card">
    <div class="card-header">
        <h3 class="card-title">Basic Data Tables with responsive plugin</h3>
    </div>
    <div class="card-body">
        <div id="demo-dt-basic_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer"><div class="row"><div class="col-sm-6"><div class="dataTables_length" id="demo-dt-basic_length"><label>Show <select name="demo-dt-basic_length" aria-controls="demo-dt-basic" class="form-control input-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div><div class="col-sm-6"><div id="demo-dt-basic_filter" class="dataTables_filter"><label>Search:<input type="search" class="form-control input-sm" placeholder="" aria-controls="demo-dt-basic"></label></div></div></div><div class="row"><div class="col-sm-12"><table id="demo-dt-basic" class="table table-striped table-bordered dataTable no-footer dtr-inline" cellspacing="0" width="100%" role="grid" aria-describedby="demo-dt-basic_info" style="width: 100%;">
            <thead>
            <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="demo-dt-basic" rowspan="1" colspan="1" style="width: 164px;" aria-sort="ascending" aria-label="Name: activate to sort column descending">Name</th><th class="sorting" tabindex="0" aria-controls="demo-dt-basic" rowspan="1" colspan="1" style="width: 249px;" aria-label="Position: activate to sort column ascending">Position</th><th class="min-tablet sorting" tabindex="0" aria-controls="demo-dt-basic" rowspan="1" colspan="1" style="width: 116px;" aria-label="Office: activate to sort column ascending">Office</th><th class="min-tablet sorting" tabindex="0" aria-controls="demo-dt-basic" rowspan="1" colspan="1" style="width: 73px;" aria-label="Extn.: activate to sort column ascending">Extn.</th><th class="min-desktop sorting" tabindex="0" aria-controls="demo-dt-basic" rowspan="1" colspan="1" style="width: 124px;" aria-label="Start date: activate to sort column ascending">Start date</th><th class="min-desktop sorting" tabindex="0" aria-controls="demo-dt-basic" rowspan="1" colspan="1" style="width: 90px;" aria-label="Salary: activate to sort column ascending">Salary</th></tr>
            </thead>
            <tbody>
            <tr role="row" class="odd">
                <td tabindex="0" class="sorting_1">Airi Satou</td>
                <td>Accountant</td>
                <td>Tokyo</td>
                <td>33</td>
                <td>2008/11/28</td>
                <td>$162,700</td>
            </tr><tr role="row" class="even">
                <td class="sorting_1" tabindex="0">Angelica Ramos</td>
                <td>Chief Executive Officer (CEO)</td>
                <td>London</td>
                <td>47</td>
                <td>2009/10/09</td>
                <td>$1,200,000</td>
            </tr><tr role="row" class="odd">
                <td tabindex="0" class="sorting_1">Ashton Cox</td>
                <td>Junior Technical Author</td>
                <td>San Francisco</td>
                <td>66</td>
                <td>2009/01/12</td>
                <td>$86,000</td>
            </tr><tr role="row" class="even">
                <td class="sorting_1" tabindex="0">Bradley Greer</td>
                <td>Software Engineer</td>
                <td>London</td>
                <td>41</td>
                <td>2012/10/13</td>
                <td>$132,000</td>
            </tr><tr role="row" class="odd">
                <td class="sorting_1" tabindex="0">Brenden Wagner</td>
                <td>Software Engineer</td>
                <td>San Francisco</td>
                <td>28</td>
                <td>2011/06/07</td>
                <td>$206,850</td>
            </tr><tr role="row" class="even">
                <td tabindex="0" class="sorting_1">Brielle Williamson</td>
                <td>Integration Specialist</td>
                <td>New York</td>
                <td>61</td>
                <td>2012/12/02</td>
                <td>$372,000</td>
            </tr><tr role="row" class="odd">
                <td class="sorting_1" tabindex="0">Bruno Nash</td>
                <td>Software Engineer</td>
                <td>London</td>
                <td>38</td>
                <td>2011/05/03</td>
                <td>$163,500</td>
            </tr><tr role="row" class="even">
                <td class="sorting_1" tabindex="0">Caesar Vance</td>
                <td>Pre-Sales Support</td>
                <td>New York</td>
                <td>21</td>
                <td>2011/12/12</td>
                <td>$106,450</td>
            </tr><tr role="row" class="odd">
                <td class="sorting_1" tabindex="0">Cara Stevens</td>
                <td>Sales Assistant</td>
                <td>New York</td>
                <td>46</td>
                <td>2011/12/06</td>
                <td>$145,600</td>
            </tr><tr role="row" class="even">
                <td tabindex="0" class="sorting_1">Cedric Kelly</td>
                <td>Senior Javascript Developer</td>
                <td>Edinburgh</td>
                <td>22</td>
                <td>2012/03/29</td>
                <td>$433,060</td>
            </tr></tbody>
        </table></div></div><div class="row"><div class="col-sm-5"><div class="dataTables_info" id="demo-dt-basic_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div></div><div class="col-sm-7"><div class="dataTables_paginate paging_simple_numbers" id="demo-dt-basic_paginate"><ul class="pagination"><li class="paginate_button previous disabled" id="demo-dt-basic_previous"><a href="#" aria-controls="demo-dt-basic" data-dt-idx="0" tabindex="0"><i class="demo-psi-arrow-left"></i></a></li><li class="paginate_button active"><a href="#" aria-controls="demo-dt-basic" data-dt-idx="1" tabindex="0">1</a></li><li class="paginate_button "><a href="#" aria-controls="demo-dt-basic" data-dt-idx="2" tabindex="0">2</a></li><li class="paginate_button "><a href="#" aria-controls="demo-dt-basic" data-dt-idx="3" tabindex="0">3</a></li><li class="paginate_button disabled" id="demo-dt-basic_ellipsis"><a href="#" aria-controls="demo-dt-basic" data-dt-idx="4" tabindex="0">…</a></li><li class="paginate_button "><a href="#" aria-controls="demo-dt-basic" data-dt-idx="5" tabindex="0">6</a></li><li class="paginate_button next" id="demo-dt-basic_next"><a href="#" aria-controls="demo-dt-basic" data-dt-idx="6" tabindex="0"><i class="demo-psi-arrow-right"></i></a></li></ul></div></div></div></div>
    </div>
</div>