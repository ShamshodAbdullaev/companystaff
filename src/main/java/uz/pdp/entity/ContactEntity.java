package uz.pdp.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ContactEntity {
    private Integer id;
    private Integer country;
    private Integer region;
    private String city;
    private Integer district;
    private String street;
    private String home;
    private String zipcode;
    private String phoneNumber;
    private String faxNumber;
    private String email;
    private String website;
}
