package uz.pdp.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompanyEntity {
    private Integer id;
    private String name;
    private String businessType;
    private Integer businessEntityId;
    private String inn;
    private String bankMfo;
    private String account;
    private String startDate;
    private Integer contactId;
}
