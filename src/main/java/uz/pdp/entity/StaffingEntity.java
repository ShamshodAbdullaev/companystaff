package uz.pdp.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StaffingEntity {
    private Integer id;
    private Integer position_id;
    private Integer company_id;
    private Double amount;
}
