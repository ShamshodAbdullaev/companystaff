package uz.pdp.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StaffEntity {
    private Integer id;
    private String fullname;
    private String gender;
    private Date birthDate;
    private String passportSeria;
    private String passportNumber;
    private String education;
    private Integer contactId;
    private Integer companyId;
    private Integer positionId;
    private Integer salaryPackageId;

}
