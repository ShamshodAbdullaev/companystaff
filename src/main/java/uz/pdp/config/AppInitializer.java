package uz.pdp.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class AppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{WebMvcConfig.class};
    }

    protected Class<?>[] getServletConfigClasses() {
        return  null;
    }

    protected String[] getServletMappings() {
        return new String[]{"/"};
    }
}
