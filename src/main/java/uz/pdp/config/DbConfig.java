package uz.pdp.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
@PropertySource("classpath:application.properties")
public class DbConfig {
    @Autowired
    Environment env;

    @Bean
    public DriverManagerDataSource dataSource(){
        DriverManagerDataSource dmd =new DriverManagerDataSource();
        dmd.setDriverClassName(env.getProperty("driver"));
        dmd.setUrl(env.getProperty("url"));
        dmd.setUsername(env.getProperty("user"));
        dmd.setPassword(env.getProperty("password"));
        return dmd;
    }
    @Bean
    public DriverManagerDataSource dataSource2(){
        DriverManagerDataSource dmd =new DriverManagerDataSource();
        dmd.setDriverClassName(env.getProperty("driver"));
        dmd.setUrl(env.getProperty("url"));
        dmd.setUsername(env.getProperty("user"));
        dmd.setPassword(env.getProperty("password"));
        return dmd;
    }
}
