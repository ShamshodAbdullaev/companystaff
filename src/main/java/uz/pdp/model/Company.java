package uz.pdp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Company {
    private Integer id;
    private String name;
    private String businessType;
    private String businessEntity;
    private String inn;
    private String bankMfo;
    private String account;
    private String startDate;
    private String country;
    private String region;
    private String city;
    private String district;
    private String street;
    private String home;
    private String zipcode;
    private String phoneNumber;
    private String faxNumber;
    private String email;
    private String website;

}
