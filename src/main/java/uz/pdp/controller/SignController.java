package uz.pdp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import uz.pdp.entity.CompanyEntity;
import uz.pdp.entity.DistrictEntity;
import uz.pdp.entity.RegionEntity;
import uz.pdp.service.CookieService;
import uz.pdp.service.DbService;
import uz.pdp.service.SignService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;

@Controller
public class SignController {
    @Autowired
    private CookieService cookieService;
    @Autowired
    private DbService dbService;

    @Autowired
    private SignService signService;

    @GetMapping("/register")
    public String getRegisterPage(Model model){
        model.addAttribute("countries",dbService.getCountries());
        model.addAttribute("businessEntities",dbService.getBusinessEntities());
        return "pages-register";
    }
    @GetMapping("/login")
    public String getLoginPage(HttpServletRequest request){
        CompanyEntity signedCompany=cookieService.getCookie(request);
        return signedCompany==null?"pages-login":"redirect:/cabinet";
    }


    @PostMapping("/signup")
    public String signUp(HttpServletRequest request, HttpServletResponse response){

        if(signService.signUp(request)) {
            if (signService.signIn(request, response)) {
                return "redirect:/cabinet";
            }else {
                return "redirect:/login";
            }
        }

//        if(signService.signUp(request)){
//            return checkUser(request,response);
//        }
        return "redirect:/register";
    }





    @GetMapping("/regions")
    @ResponseBody
    public ArrayList<RegionEntity> getRegions(HttpServletRequest request){
        Integer countryId=Integer.valueOf(request.getParameter("id"));
        return dbService.getRegions(countryId);
    }


    @GetMapping("/districts")
    @ResponseBody
    public ArrayList<DistrictEntity> getDistricts(HttpServletRequest request)  {
        Integer regionId=Integer.valueOf(request.getParameter("id"));
        return dbService.getDistricts(regionId);
        //BU YUQORIDAGI METODNING IKKINCHI USULI
        // @GetMapping("/districts")
        //    @ResponseBody
        //    public ArrayList<DistrictEntity> getDistricts(@RequestParam("id") Integer regionId){
        //        return dbService.getDistricts(regionId);
        //    }
    }




//    @PostMapping("/login")
//    public String getLoginPageP(HttpRequest request){
//        return "pages-login";
//    }

}
