package uz.pdp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import uz.pdp.entity.BusinessEntity;
import uz.pdp.entity.CompanyEntity;
import uz.pdp.entity.ContactEntity;
import uz.pdp.entity.RegionEntity;
import uz.pdp.model.Company;
import uz.pdp.model.CompanyStructure;
import uz.pdp.service.CookieService;
import uz.pdp.service.DbService;
import uz.pdp.service.SignService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;

@Controller
public class CabinetController {

    @Autowired
    private CookieService cookieService;

    @Autowired
    private SignService signService;

    @Autowired
    private DbService dbService;

    @GetMapping("/cabinet")
    public ModelAndView getCabinet(ModelAndView modelAndView) {
        modelAndView.addObject("view", "home");
        return modelAndView;
    }

//    @GetMapping("/cabinet/{page}")
//    public ModelAndView getCabinetAbout(@PathVariable String page, ModelAndView modelAndView) {
//        modelAndView.addObject("view", page);
//        return modelAndView;
//    }

    @PostMapping("/cabinet")
    public String checkUser(HttpServletRequest request, HttpServletResponse response) {

        return signService.signIn(request,response)?"redirect:/cabinet":"redirect:/login";
    }

    @GetMapping("/cabinet/about")
    public ModelAndView getCabinetAbout(HttpServletRequest request, ModelAndView modelAndView){
        CompanyEntity companyEntity = cookieService.getCookie(request);
        Company company=dbService.getCompany(companyEntity.getId());
//        BusinessEntity businessEntity=dbService.getBusinessEntity(company.getBusinessEntityId());
//        ContactEntity contact=dbService.getContact(company.getContactId());
        modelAndView.addObject("view","about");
        modelAndView.addObject("company",company);
//        modelAndView.addObject("businessEntity",businessEntity);
//        modelAndView.addObject("contact",contact);
        return modelAndView;
    }
    @GetMapping("/logout")
    public String logout(HttpServletRequest request,HttpServletResponse response){
        signService.logOut(request, response);
        return "redirect:/login";
    }

    @GetMapping("/cabinet/structure")
    public ModelAndView getStructure(ModelAndView modelAndView,HttpServletRequest request ){
        Integer id=cookieService.getCookie(request).getId();
        modelAndView.addObject("view","structure");
        modelAndView.addObject("structure",dbService.getStaffing(id));
        CompanyEntity companyEntity = cookieService.getCookie(request);
        Company company=dbService.getCompany(companyEntity.getId());
        modelAndView.addObject("company",company);
        return modelAndView;
    }


    @GetMapping("/cabinet/staff")
    public ModelAndView getCabinetStaff(ModelAndView modelAndView){
        modelAndView.addObject("view","staff");
        return modelAndView;
    }
//
//    @GetMapping("/cabinet/structure")
//    public ModelAndView getCabinetStructure(ModelAndView modelAndView){
//        modelAndView.addObject("view","structure");
//        return modelAndView;
//    }
//
//    @GetMapping("/cabinet/vacancy")
//    public ModelAndView getCabinetVacancy(ModelAndView modelAndView){
//        modelAndView.addObject("view","vacancy");
//        return modelAndView;
//    }
}
