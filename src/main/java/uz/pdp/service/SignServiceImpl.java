package uz.pdp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.sql.*;

@Service
public class SignServiceImpl implements SignService {

    @Autowired
    DataSource dataSource;

    @Autowired
    CookieService cookieService;

    @Override
    public boolean signIn(HttpServletRequest request, HttpServletResponse response) {
        String username = request.getParameter("login");
        String password = request.getParameter("password");
        try (Connection connection = dataSource.getConnection()) {
            if (connection != null) {
                String sql = "select count(*) from company " +
                        "where login='" + username + "' and password='" + password + "';";
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(sql);
                rs.next();
                int count = rs.getInt(1);
                if (count > 0) {
                    cookieService.createCookie(request, response);
                    return true;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean signUp(HttpServletRequest request) {

        String login = request.getParameter("login");
        String password = request.getParameter("password");
        String prepassword = request.getParameter("prepassword");
        String name = request.getParameter("name");
        String businessType = request.getParameter("business_type");
        Integer businessEntity = Integer.valueOf(request.getParameter("business_entity"));
        String inn = request.getParameter("inn");
        String bankMfo = request.getParameter("bank_mfo");
        String account = request.getParameter("account");
        String startDate = request.getParameter("start_date");
        String country = request.getParameter("country");
        String region = request.getParameter("region");
        String city = request.getParameter("city");
        String district = request.getParameter("district");
        String street = request.getParameter("street");
        String home = request.getParameter("home");
        String phoneNumber = request.getParameter("phone_number");
        String faxNumber = request.getParameter("fax_number");
        String zipCode = request.getParameter("zip_code");
        String email = request.getParameter("email");
        String website = request.getParameter("website");

        try (Connection connection = dataSource.getConnection()) {
            if (connection != null) {
                String countSql = "select count(*) from company where login='" + login + "'" +
                        " or inn='" + inn + "' or name='" + name + "';";
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(countSql);
                rs.next();
                Integer count = rs.getInt(1);
                if (count == 0) {
                    String contactSql = "insert into contact(country,region,city,district," +
                            "street,home,zipcode,phone_number,fax_number,email,website)" +
                            "values('" + country + "','" + region + "','" + city + "','" + district + "','" + street + "','" +
                            home + "','" + zipCode + "','" + phoneNumber + "','" + faxNumber + "','" + email + "','" + website + "');";
                    if (!statement.execute(contactSql)) {

                        ResultSet rs1 = statement.executeQuery("select id from contact where email='" + email + "';");
                        rs1.next();
                        int contactId = rs1.getInt(1);
                        String companySql = "insert into company(name,business_type,business_entity_id," +
                                "inn,bank_mfo,account, start_date, contact_id, login,password) " +
                                "values('" + name + "', '" + businessType + "', " + businessEntity + ", '" + inn + "', '"
                                + bankMfo + "', '" + account + "', '" + startDate + "', " + contactId + ", '" + login + "', '" + password + "');";
                        if (!statement.execute(companySql)) {
                            return true;
                        }
                    }
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void logOut(HttpServletRequest request,HttpServletResponse response) {
        cookieService.deleteCookie(request,response);
    }
}
