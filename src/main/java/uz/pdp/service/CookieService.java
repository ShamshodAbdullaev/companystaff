package uz.pdp.service;

import uz.pdp.entity.CompanyEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface CookieService {
    void createCookie(HttpServletRequest request, HttpServletResponse response);
    void deleteCookie(HttpServletRequest request,HttpServletResponse response);
    CompanyEntity getCookie(HttpServletRequest request);

}
