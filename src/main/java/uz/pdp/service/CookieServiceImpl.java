package uz.pdp.service;

import com.google.gson.Gson;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.entity.CompanyEntity;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Service
public class CookieServiceImpl implements CookieService{
    @Autowired
    DataSource dataSource;

    private Gson gson=new Gson();
    private Base64 base64=new Base64();
    private static final String cookieName="company.staff";

    @Override
    public void createCookie(HttpServletRequest request, HttpServletResponse response) {
        CompanyEntity company = new CompanyEntity();
        try (Connection connection=dataSource.getConnection()){
            if (connection!=null){
                String sql="select * from company " +
                        "where login='"+request.getParameter("login")+"';";
                Statement statement=connection.createStatement();
                ResultSet rs=statement.executeQuery(sql);
                rs.next();
                company.setId(rs.getInt("id"));
                company.setName(rs.getString("name"));
                company.setBusinessType(rs.getString("business_type"));
                company.setBusinessEntityId(rs.getInt("business_entity_id"));
                company.setInn(rs.getString("inn"));
                company.setBankMfo(rs.getString("bank_mfo"));
                company.setAccount(rs.getString("account"));
                company.setStartDate(rs.getString("start_date"));
                company.setContactId(rs.getInt("contact_id"));
                String cookieValue=gson.toJson(company);
                cookieValue=new String(base64.encode(cookieValue.getBytes()));
                Cookie cookie=new Cookie(cookieName,cookieValue);
                response.addCookie(cookie);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteCookie(HttpServletRequest request,HttpServletResponse response) {
        Cookie[]cookies=request.getCookies();
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals(cookieName)){
               cookie.setMaxAge(0);
               response.addCookie(cookie);
            }
        }

    }

    @Override
    public CompanyEntity getCookie(HttpServletRequest request) {
        CompanyEntity company=new CompanyEntity();
        Cookie[]cookies=request.getCookies();
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals(cookieName)){
               String cookieValue=cookie.getValue();
               cookieValue = new String(base64.decode(cookieValue.getBytes()));
               company=gson.fromJson(cookieValue,CompanyEntity.class);
               return company;
            }

        }
        return null;
    }
}
