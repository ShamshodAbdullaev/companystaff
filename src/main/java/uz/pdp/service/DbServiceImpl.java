package uz.pdp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.entity.*;
import uz.pdp.model.Company;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

@Service("DbService")
public class DbServiceImpl implements DbService {
    

    @Autowired
    DataSource dataSource;

    @Override
    public Company getCompany(Integer id) {
        BusinessEntity businessEntity;
        ContactEntity contactEntity;
        CountryEntity countryEntity;
        RegionEntity regionEntity;
        DistrictEntity districtEntity;
        Company company =new Company();
        try (Connection connection=dataSource.getConnection()){
            String sql="select * from company where id="+id+";";
            Statement statement=connection.createStatement();
            ResultSet rs=statement.executeQuery(sql);
            rs.next();
            company.setId(rs.getInt("id"));
            company.setName(rs.getString("name"));
            company.setBusinessType(rs.getString("business_type"));
            int bussinessEntityId=rs.getInt("business_entity_id");
            businessEntity=getBusinessEntity(bussinessEntityId);
            company.setBusinessEntity(businessEntity.getType());
            company.setInn(rs.getString("inn"));
            company.setBankMfo(rs.getString("bank_mfo"));
            company.setAccount(rs.getString("account"));
            company.setStartDate(rs.getString("start_date"));
            int contactId=rs.getInt("contact_id");
            contactEntity=getContact(contactId);
            countryEntity=getCountry(contactEntity.getCountry());
            regionEntity=getRegion(contactEntity.getRegion());
            districtEntity=getDistrict(contactEntity.getDistrict());
            company.setCountry(countryEntity.getName());
            company.setRegion(regionEntity.getName());
            company.setDistrict(districtEntity.getName());
            company.setCity(contactEntity.getCity());
            company.setStreet(contactEntity.getStreet());
            company.setHome(contactEntity.getHome());
            company.setPhoneNumber(contactEntity.getPhoneNumber());
            company.setFaxNumber(contactEntity.getFaxNumber());
            company.setZipcode(contactEntity.getZipcode());
            company.setEmail(contactEntity.getEmail());
            company.setWebsite(contactEntity.getWebsite());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return company;
    }

    @Override
    public BusinessEntity getBusinessEntity(Integer id) {
        BusinessEntity businessEntity=new BusinessEntity();
        try(Connection connection=dataSource.getConnection()) {
            String sql="select * from business_entity where id="+id+";";
            Statement statement=connection.createStatement();
            ResultSet rs=statement.executeQuery(sql);
            rs.next();
            businessEntity.setId(rs.getInt("id"));
            businessEntity.setType(rs.getString("type"));
            businessEntity.setDescription(rs.getString("description"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return businessEntity;
    }

    @Override
    public ContactEntity getContact(Integer id) {
        ContactEntity contact = new ContactEntity();
        try(Connection connection=dataSource.getConnection()) {
            String sql="select * from contact where id="+id+";";
            Statement statement=connection.createStatement();
            ResultSet rs=statement.executeQuery(sql);
            rs.next();
            contact.setId(rs.getInt("id"));
            contact.setCountry(rs.getInt("country"));
            contact.setRegion(rs.getInt("region"));
            contact.setCity(rs.getString("city"));
            contact.setDistrict(rs.getInt("district"));
            contact.setStreet(rs.getString("street"));
            contact.setHome(rs.getString("home"));
            contact.setZipcode(rs.getString("zipcode"));
            contact.setPhoneNumber(rs.getString("phone_number"));
            contact.setFaxNumber(rs.getString("fax_number"));
            contact.setEmail(rs.getString("email"));
            contact.setWebsite(rs.getString("website"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return contact;
    }

    @Override
    public StaffEntity getStaff(Integer id) {
        return null;
    }

    @Override
    public CountryEntity getCountry(Integer id) {
        CountryEntity country=new CountryEntity();
        try(Connection connection=dataSource.getConnection()) {
            String sql="select * from country where id="+id+";";
            Statement statement=connection.createStatement();
            ResultSet rs=statement.executeQuery(sql);
            rs.next();
            country.setId(rs.getInt("id"));
            country.setName(rs.getString("name"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return country;
    }

    @Override
    public RegionEntity getRegion(Integer id) {
        RegionEntity region=new RegionEntity();
        try(Connection connection=dataSource.getConnection()) {
            String sql="select * from region where id="+id+";";
            Statement statement=connection.createStatement();
            ResultSet rs=statement.executeQuery(sql);
            rs.next();
            region.setId(rs.getInt("id"));
            region.setName(rs.getString("name"));
            region.setCountryId(rs.getInt("country_id"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return region;
    }

    @Override
    public DistrictEntity getDistrict(Integer id) {
        DistrictEntity district=new DistrictEntity();
        try(Connection connection=dataSource.getConnection()) {
            String sql="select * from district where id="+id+";";
            Statement statement=connection.createStatement();
            ResultSet rs=statement.executeQuery(sql);
            rs.next();
            district.setId(rs.getInt("id"));
            district.setName(rs.getString("name"));
            district.setRegionId(rs.getInt("region_id"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return district;
    }

    @Override
    public ArrayList<CountryEntity> getCountries() {
        ArrayList<CountryEntity> countries=new ArrayList<>();
        CountryEntity country;
        try (Connection connection=dataSource.getConnection()){
            String sql="select * from country";
            Statement statement=connection.createStatement();
            ResultSet rs=statement.executeQuery(sql);
            while (rs.next()) {
                country=new CountryEntity();
                country.setId(rs.getInt("id"));
                country.setName(rs.getString("name"));
                countries.add(country);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return countries;
    }

    @Override
    public ArrayList<RegionEntity> getRegions(Integer countryId) {
        ArrayList<RegionEntity> regions=new ArrayList<>();
        RegionEntity region ;
        try (Connection connection=dataSource.getConnection()){
            String sql="select * from region where country_id="+countryId+";";
            Statement statement=connection.createStatement();
            ResultSet rs=statement.executeQuery(sql);
            while (rs.next()) {
                region = new RegionEntity();
                region.setId(rs.getInt("id"));
                region.setName(rs.getString("name"));
                region.setCountryId(rs.getInt("country_id"));
                regions.add(region);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return regions;
    }

    @Override
    public ArrayList<DistrictEntity> getDistricts(Integer regionId) {
        ArrayList<DistrictEntity> districts = new ArrayList<>();
        DistrictEntity district;
        try (Connection connection = dataSource.getConnection()) {
            String sql = "select * from district where region_id=" + regionId + ";";
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                district = new DistrictEntity();
                district.setId(rs.getInt("id"));
                district.setName(rs.getString("name"));
                district.setRegionId(rs.getInt("region_id"));
                districts.add(district);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return districts;
    }

    @Override
    public ArrayList<BusinessEntity> getBusinessEntities() {
        ArrayList<BusinessEntity> businessEntities=new ArrayList<>();
        BusinessEntity businessEntity;
        try (Connection connection=dataSource.getConnection()){
            String sql="select * from business_entity";
            Statement statement=connection.createStatement();
            ResultSet rs=statement.executeQuery(sql);
            while (rs.next()) {
                businessEntity=new BusinessEntity();
                businessEntity.setId(rs.getInt("id"));
                businessEntity.setType(rs.getString("type"));
                businessEntity.setDescription(rs.getString("description"));
                businessEntities.add(businessEntity);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return businessEntities;
    }

    @Override
    public HashMap<String, Double> getStaffing(Integer companyId) {
        HashMap<String,Double> structure=new HashMap<>();
        try(Connection connection=dataSource.getConnection()) {
            String sql="select * from staffing where id="+companyId+";";
            Statement statement=connection.createStatement();
            ResultSet rs=statement.executeQuery(sql);
            while (rs.next()){
                Integer position_id=rs.getInt("position_id");
                structure.put(getPosition(position_id).getName(),rs.getDouble("amount"));

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return structure;
    }

    @Override
    public PositionEntity getPosition(Integer positionId) {
        PositionEntity position=new PositionEntity();
        try(Connection connection=dataSource.getConnection()) {
            String sql="select * from position where id="+positionId+";";
            Statement statement=connection.createStatement();
            ResultSet rs=statement.executeQuery(sql);
            rs.next();
            position.setId(rs.getInt("id"));
            position.setName(rs.getString("name"));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }
}
