package uz.pdp.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface SignService {
    boolean signIn(HttpServletRequest request, HttpServletResponse response);
    boolean signUp(HttpServletRequest request);
    void logOut(HttpServletRequest request,HttpServletResponse response);
}
