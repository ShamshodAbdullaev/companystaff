package uz.pdp.service;

import uz.pdp.entity.*;
import uz.pdp.model.Company;

import java.util.ArrayList;
import java.util.HashMap;

public interface DbService {
    Company getCompany(Integer id);
    BusinessEntity getBusinessEntity(Integer id);
    ContactEntity getContact(Integer id);
    StaffEntity getStaff(Integer id);
    CountryEntity getCountry(Integer id);
    RegionEntity getRegion(Integer id);
    DistrictEntity getDistrict(Integer id);
    ArrayList<CountryEntity> getCountries();
    ArrayList<RegionEntity> getRegions(Integer countryId);
    ArrayList<DistrictEntity> getDistricts(Integer regionId);
    ArrayList<BusinessEntity> getBusinessEntities();
    HashMap<String, Double> getStaffing(Integer companyId);
    PositionEntity getPosition(Integer positionId);

}
