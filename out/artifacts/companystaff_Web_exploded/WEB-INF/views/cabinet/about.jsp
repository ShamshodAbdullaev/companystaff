<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--<div class="panel panel-body text-center">--%>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header text-center">
                        <h1>UzPdpBootCamp Mchj </h1>
                    </div>
                    <div class="card-body text-left " style="font-size: 20px">
                        <div class="row" style="margin-bottom: 10px">
                            <div class="col-md-6 "><b><spring:message code="company.business_type"/> </b><span>Programming</span></div>
                            <div class="col-md-6 "><b><spring:message code="company.inn"/>: </b><span>097213516</span></div>
                        </div>
                        <div class="row " style="margin-bottom: 10px">
                            <div class="col-md-6"><b><spring:message code="company.mfo"/>: </b><span>01001</span></div>
                            <div class="col-md-6"><b><spring:message code="company.account"/>: </b><span>20208000709721351601</span></div>
                        </div>
                        <div class="row" style="margin-bottom: 10px">
                            <div class="col-md-6"><b><spring:message code="company.start_date"/>: </b><span>01.01.1999</span></div>
                            <div class="col-md-6"><b><spring:message code="company.director"/>: </b><span>O.Mirzayev</span></div>
                        </div>
                        <div class="row" style="margin-bottom: 10px">
                            <div class="col-md-6"><b><spring:message code="company.staff_number"/>: </b><span>50</span></div>
                            <div class="col-md-6"><b><spring:message code="company.vacancies"/>: </b><span>5</span></div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <h3><spring:message code="company.adress"/></h3>
                            </div>
                            <div class="card-body">
                                <p>Uzbekiston,Tosh,Mirobod,Afrosiyob-4a</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<%--</div>--%>