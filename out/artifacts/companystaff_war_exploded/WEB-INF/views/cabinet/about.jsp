<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--<div class="panel panel-body text-center">--%>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header text-center">
                        <h1>"${company.name}" ${company.businessEntity} </h1>
                    </div>

                    <div class="card-body text-left " style="font-size: 20px">
                        <div class="row" style="margin-bottom: 10px">
                            <div class="col-md-6 "><b><spring:message code="company.business_type"/> </b><span>${company.businessType}</span></div>
                            <div class="col-md-6 "><b><spring:message code="company.inn"/>: </b><span>${company.inn}</span></div>
                        </div>
                        <div class="row " style="margin-bottom: 10px">
                            <div class="col-md-6"><b><spring:message code="company.mfo"/>: </b><span>${company.bankMfo}</span></div>
                            <div class="col-md-6"><b><spring:message code="company.account"/>: </b><span>${company.account}</span></div>
                        </div>
                        <div class="row" style="margin-bottom: 10px">
                            <div class="col-md-6"><b><spring:message code="company.start_date"/>: </b><span>${company.startDate}</span></div>
                            <div class="col-md-6"><b><spring:message code="company.director"/>: </b><span>O.Mirzayev</span></div>
                        </div>
                        <div class="row" style="margin-bottom: 10px">
                            <div class="col-md-6"><b><spring:message code="company.staff_number"/>: </b><span>50</span></div>
                            <div class="col-md-6"><b><spring:message code="company.vacancies"/>: </b><span>5</span></div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <h3><spring:message code="company.adress"/></h3>
                            </div>
                            <div class="card-body">
                                <p>
                                    ${company.country}, ${company.region}, ${company.city}, ${company.district},
                                    ${company.street}-${company.home}, index:${company.zipcode} <br>tel:${company.phoneNumber},
                                    fax:${company.faxNumber}, email:${company.email}, website:${company.website}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<%--</div>--%>